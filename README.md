# Backup
A collection of bash shell helper scripts to assist with; system and data backups,
backup management and system and data restores.

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

## Acknowledgement
This README brought to you courtesy of the [Acme Readme Corp.][1]

----------------------------------------------------------------------------------------

## Overview
### Approach
The basic approach it to backup each partition (filesystem) individually, where
partitions are regarded as being of one of two types; either a 'system' partition or a
'data' partition. Note that it is actually the filesystem that is backed up rather then
the partition directly.

### System
System filesystems are backed up using ['fsarchiver'][2] via "system-backups.sh". It is
intended for use with disks formatted with [GUID Partition Tables][3] and, prior to
backing up the filesystems, uses ['sgdisk'][4] to save a copy of the partition table.

### Data

Data filesystems are backed up using ['duply'][5] via "data-backups.sh".

----------------------------------------------------------------------------------------

## Installation
Simply clone the repo to a suitable local directory and, if required, move the scripts
to a folder in your path. One option is to have a 'bin' folder in your home directory
[which is also added to $PATH][6]. The '~/bin' directory can then have links setup to
the script in question, e.g.

    ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    (22:44:04)~]$ ls -l ~/bin
    total 0
        :
    lrwxrwxrwx 1 swillber users 52 Jul  3 10:52 backup-status -> /home/swillber/Codebase/bash/Backup/backup-status.sh
    lrwxrwxrwx 1 swillber users 53 Jul  3 17:04 backup-summary -> /home/swillber/Codebase/bash/Backup/backup-summary.sh
    lrwxrwxrwx 1 swillber users 51 Jul  3 17:06 data-backups -> /home/swillber/Codebase/bash/Backup/data-backups.sh
    lrwxrwxrwx 1 swillber users 53 Jul  3 18:22 system-backups -> /home/swillber/Codebase/bash/Backup/system-backups.sh
    lrwxrwxrwx 1 swillber users 53 Jul  3 18:24 system-restore -> /home/swillber/Codebase/bash/Backup/system-restore.sh
        :
    (22:44:08)~]$
    ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

which allows the scripts to be easily invoked.

*NB for the link name it is convenient to drop the '.sh' suffix.*

----------------------------------------------------------------------------------------

## Usage
As these are just shell scripts they can just be invoked from the command line with the
appropriate arguments (see fnUsage() function in each script).

----------------------------------------------------------------------------------------

## Reporting bugs
Please use the [GitHub issue tracker][7] for any bugs or feature suggestions.

----------------------------------------------------------------------------------------

## Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

Contributions must be licensed under the GNU GPLv3.
The contributor retains the copyright.

----------------------------------------------------------------------------------------

## License
This project is licensed under the GNU General Public License, v3. A copy of this license
is included in the file [LICENSE.md](LICENSE.md).

  [1]: https://gist.github.com/zenorocha/4526327
  [2]: http://www.fsarchiver.org/
  [3]: https://en.wikipedia.org/wiki/GUID_Partition_Table
  [4]: https://www.cyberciti.biz/faq/linux-backup-restore-a-partition-table-with-sfdisk-command/
  [5]: http://duply.net/wiki/index.php/Duply-documentation
  [6]: https://askubuntu.com/questions/60218/how-to-add-a-directory-to-the-path
  [7]: https://github.com/WhowantsToknow/Backup/issues
