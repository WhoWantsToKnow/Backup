#!/bin/bash
# ==================================================================================== #
#         FILE: data-backup-status.sh                                                  #
# ==================================================================================== #
#  DESCRIPTION: Runs duply to produce a status report for each profile directory found #
#               in the '.duply' directory (excluding 'example') and consolidates the   #
#               output into one large array. It then processess the array to create an #
#               overall summary report for all profiles found.                         #
# ==================================================================================== #
#        USAGE: data-backup-status.sh (no arguments expected or accepted)              #
#        NEEDS: duply                                                                  #
#    COPYRIGHT: Keith Watson (swillber@gmail.com)                                      #
#      VERSION: 1.4.0                                                                  #
# DATE CREATED: 24-Feb-2017                                                            #
# LAST CHANGED: 27-Dec-2022 22:34                                                      #
#   KNOWN BUGS: ---                                                                    #
# ==================================================================================== #
#  LICENSE:                                                                            #
#  This program is free software; you can redistribute it and/or modify it under the   #
#  terms of the GNU General Public License as published by the Free Software           #
#  Foundation; either version 2 of the License, or (at your option) any later version. #
#                                                                                      #
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY     #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A     #
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.           #
#                                                                                      #
#  You should have received a copy of the GNU General Public License along with this   #
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,   #
#  Fifth Floor, Boston, MA 02110=1301, USA.                                            #
#                                                                                      #
#  Copyright 2016 Keith Watson <swillber@gmail.com>                                    #
# ==================================================================================== #
#                                    F U N C T I O N S                                 #
# ==================================================================================== #
# ------------------------------------------------------------------------------------ #
function fnGetStatusReports () {
# ------------------------------------------------------------------------------------ #
# For each profile in the profiles list runs the duply status command and appends the  #
# output to the global StatusReports array.                                            #
# Arguments:                                                                           #
#   none                                                                               #
# Returns:                                                                             #
#   true (0) if all profiles found, false (1) if one or more profiles not found.       #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r DUPLYCMD="status"
    declare -r STDERR="/tmp/${SCRIPTNAME}${$}.wrk"
    declare -r PROFILESNOTFOUND="Profiles not found:"
    declare -r PROFILESFOUND="Profiles found:"
    # extract list of profile directories from the duply config directory
    # NB trailing slash on '.duply' directory means symbolic links are followed
    declare -a PROFILES=($(ls -1F ~/.duply/ | grep "/$" | grep -v example | sed "s/\///"))
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -a MissingProfiles
    declare -a FoundProfiles
    declare    Error=""
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    IFS=$'\n'                # delete field separators so that whole lines are not split
    for Profile in "${PROFILES[@]}"; do
        # append each duply command output to the array and capture stderr output
        mapfile -O ${#StatusReports[@]} StatusReports < \
                        <(duply "${Profile}" "${DUPLYCMD}" 2>"${STDERR}")
        Error=$(<"${STDERR}")  # get stderr output into variable
        if [[ -n ${Error} ]]; then
            MissingProfiles+=(" ${Profile}")
        else
            FoundProfiles+=(" ${Profile}")
        fi
    done
    if [[ ${#FoundProfiles[@]} > 0 ]]; then
        fnPrint "${PROFILESFOUND}" "${FoundProfiles[@]}" "${NL}" "${NL}"
    fi
    if [[ ${#MissingProfiles[@]} > 0 ]]; then
        fnPrint "${NL}" "${PROFILESNOTFOUND}" "${MissingProfiles[@]}" "${NL}" "${NL}"
        Result=${FALSE}
    fi
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnPrintProfileHeader () {
# ------------------------------------------------------------------------------------ #
# Print the initial header information for a new profile list of backup sets. The      #
# profile name is extracted from the report line supplied as an argument.              #
# Arguments:                                                                           #
#   1) report line containing profile name.                                            #
# Returns:                                                                             #
#   none                                                                               #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r  HEADING1="$(tr -d '\n' <<EOSL
${BOLDGREEN}  Backup Type:            Run Date & Time:
    Volumes:       Size (Mb)   Description:${NOCOLOUR}
EOSL
)"
    declare -r  HEADING2="$(tr -d '\n' <<EOSL
${BOLDGREEN}  ============    ========================
    ========    ============   =========================================================
===========${NOCOLOUR}
EOSL
)"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare     ReportLine="${@}"
    declare     NewProfile=""
    # ---------------------------------- PROCEDURE ----------------------------------- #
    Profile="${ReportLine:${#STARTOFPROFILE}:-2}"
    NewProfile="${BOLDCYAN}${Profile} $(printf "~%.0s" {1..160})"
    NewProfile="${NewProfile::150}"
    NewProfile+="${NOCOLOUR}"
    fnPrint "${NewProfile}${NL}"
    fnPrint "${HEADING1}${NL}"
    fnPrint "${HEADING2}${NL}"
}
# ------------------------------------------------------------------------------------ #
function fnUpdateHashes () {
# ------------------------------------------------------------------------------------ #
# Processes a manifest file description report line to extract the file path and name, #
# then greps the file to capture any description associated with the backup. The       #
# description is added to an associative array keyed by the date in epoch format. The  #
# date is extracted from the file name.                                                #
#                                                                                      #
# Also gets the total size in bytes of all the files in the backup set.                #
#                                                                                      #
# Arguments: manifest file description (path+filename)                                 #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r MANIFEST="${@}"
    declare -r DESCHDR="# Description: "
    declare -r FULL="duplicity-full"
    declare -r INCR="duplicity-inc"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -a ReportPart
    declare    BackupType
    declare    FileName
    declare    EndTime
    declare    EpochTime
    declare    Comment
    declare -i Result=0
    # ---------------------------------- PROCEDURE ----------------------------------- #
    FileName="$(basename "${MANIFEST}")" # get filename
    # depending on the type of backup extract end date/time from file name
    BackupType="$(cut -d. -f1 <<<"${FileName}")"
    if [[ "${BackupType}" == "${FULL}" ]]; then
        EndTime="$(cut -d. -f2 <<<"${FileName}")"
    else
        EndTime="$(cut -d. -f4 <<<"${FileName}")"
    fi
    # convert to seconds since the Epoch (1970-01-01 00:00 UTC)
    EpochTime=$(date -d "${EndTime::8} ${EndTime:9:2}:${EndTime:11:2}:${EndTime:13:2} GMT" +"%s")
    # get any description text in manifest file
    Comment=$(grep -E "${DESCHDR}" "${BackupsPath}/${FileName}")
    # if found remove the prefix which identifies it
    if ! [[ -z "${Comment}" ]]; then
        Comment="${Comment#${DESCHDR}}"
        # then add it to the array
        Description["${EpochTime}"]="${Comment}"
    fi
    # add the size of the backup set as an associative array item
    BackupSize["${EpochTime}"]="$(fnSumBackupSizes "${BackupType}" "${EndTime}")"
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnSumBackupSizes () {
# ------------------------------------------------------------------------------------ #
# Get the total size in bytes of all the files in the backup set specified by the      #
# supplied arguments.                                                                  #
#                                                                                      #
# Arguments:                                                                           #
#   1) string value can be "full" or "incremental"                                     #
#   2) date time string extracted from the manifest file name                          #
#                                                                                      #
# Returns:                                                                             #
#   string - total size in bytes of all files in the backup set                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    TYPE="${1}"
    TIME="${2}"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    TotalSize=0
    declare -i Result=0
    # ---------------------------------- PROCEDURE ----------------------------------- #
    for Size in $(find "${BackupsPath}" -name "*${TYPE}*${TIME}*" -printf "%s\n"); do
        TotalSize=$((TotalSize+Size))
    done
    printf "%s" "${TotalSize}"
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnGetBackupSize () {
# ------------------------------------------------------------------------------------ #
# Returns the backup set size saved in the BackupSize array.                           #
#                                                                                      #
# Arguments: current report line                                                       #
# Returns: size of backup set                                                         #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    Line="${@}"
    declare    DateString
    declare    Epoch
    declare    Size
    declare -i Result=0
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # get the date from the report line and convert to no. seconds since epoch
    DateString="${Line:29:24}"
    Epoch="$(date -d "${DateString}" +"%s")"
    # get the backup size value
    Size="${BackupSize[${Epoch}]}"
    # if there's no size value, return zero
    if [[ -z "${Size}" ]]; then
        Size=0
    fi
    # output return value
    fnPrint "${Size}"
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnAddDescription () {
# ------------------------------------------------------------------------------------ #
# Adds any comments saved in the Description array that apply to this report line to   #
# the end of that line.                                                                #
#                                                                                      #
# Arguments: current report line                                                       #
# Returns: updated report line                                                         #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    Line="${@}"
    declare    DateString
    declare    Epoch
    declare -i Result=0
    # ---------------------------------- PROCEDURE ----------------------------------- #
    # get the date from the report line and convert to no. seconds since epoch
    DateString="${Line:29:24}"
    Epoch="$(date -d "${DateString}" +"%s")"
    # see if there's a corresponding description and append it
    if [[ -z "${Description[${Epoch}]}" ]]; then
        Line+="   <none>"
    else
        Line+="   ${Description[${Epoch}]}"
    fi
    # output return value
    fnPrint "${Line}${NL}"
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnFormatLine () {
# ------------------------------------------------------------------------------------ #
# Reformats a report line to remove unwanted spaces.                                   #
#                                                                                      #
# Arguments: current report line                                                       #
# Returns: updated report line                                                         #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    Line="${@}"
    declare    Type="${Line:9:11}"
    declare    Date="${Line:29:24}"
    declare    Vol="${Line:67:4}"
    declare    Desc="${Line:74}"
    declare -i Result=0
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnPrint "   ${Type}    ${Date}        ${Vol}    ${Desc}"
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnPrint () {
# ------------------------------------------------------------------------------------ #
# output the arguments using the printf builtin.                                       #
# Arguments:                                                                           #
#   1) data to be displayed                                                            #
# Returns:                                                                             #
#   none                                                                               #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    printf "%b" "${@}"
}
# ==================================================================================== #
#                                       M A I N                                        #
# ==================================================================================== #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
declare -r NL=$'\n'
declare -r SCRIPTNAME="${0##*/}"                        # base name of script
declare -r SCALE=1000000
declare -ir TRUE=0
declare -ir FALSE=1
# shellcheck disable=SC2034
# SC2034: 'variable' appears unused. Verify it or export it.
# ..............................console colour codes.................................. #
{ # code block so shellcheck directive applies to all declare statements
    declare -r BLACK="\e[0;30m"                # foreground colours
    declare -r RED="\e[0;31m"
    declare -r GREEN="\e[0;32m"
    declare -r BROWN="\e[0;33m"
    declare -r BLUE="\e[0;34m"
    declare -r PURPLE="\e[0;35m"
    declare -r CYAN="\e[0;36m"
    declare -r GRAY="\e[0;37m"
    declare -r DARKGRAY="\e[1;30m"             # bold foreground colours
    declare -r BOLDRED="\e[1;31m"
    declare -r BOLDGREEN="\e[1;32m"
    declare -r YELLOW="\e[1;33m"
    declare -r BOLDBLUE="\e[1;34m"
    declare -r BOLDPURPLE="\e[1;35m"
    declare -r BOLDCYAN="\e[1;36m"
    declare -r WHITE="\e[1;37m"
    declare -r NOCOLOUR="\e[0m"                # transparent - don't change
    declare -r UP1="$(tput cuu 1)"             # Move N lines up
}
# ...........................runtime message texts.................................... #
declare -r RUNSTARTED="\n${WHITE}Backup Status Report : "
declare -r RUNFAILED="$(cat <<EOSL
${WHITE}### Backup status report finished - no reports possible ###${NOCOLOUR}\n\n
EOSL
)"
declare -r PROFILEERROR="\n${BOLDRED}### NB Some profiles not found ###${NOCOLOUR}\n"
declare -r RUNENDED="\n${WHITE}=== Backup status report finished ===${NOCOLOUR}\n\n"
# .................................report line types.................................. #
declare -r STARTOFPROFILE="Using profile '/home/swillber/.duply/"
declare -r ARGS="Args: "
declare -r ARGPREFIX="*file://"
declare -r STARTOFBACKUPSET=" Type of backup set:"
declare -r ENDOFBACKUPSET="-------------------------"
declare -r ISINCREMENT="Incremental"
declare -r ISFULL="Full"
declare -r CHAINSTARTTIME="Chain start time"
# -------------------------------- GLOBAL VARIABLES ---------------------------------- #
declare -a StatusReports
declare -A Description                 # holds backup comments keyed by date (epoch)
declare -A BackupSize                  # holds backup set size keyed by date (epoch)
declare -A ChainTotal                  # holds overall chain backup set size keyed by date
declare    BackupsPath                 # path to location of backups partition
declare    Profile                     # current profile set by fnPrintProfileHeader
declare    Printing=${FALSE}           # flag to indicate if report lines to be printed
declare    Size                        # holds the overall size of an individual backup
declare -i BackupTotal=0               # holds the total for each backup set
declare -i ProfileTotal=0              # holds the overall total for the prefix
declare -i GrandTotal=0                # holds the overall total for the run
declare    MissingProfiles=${FALSE}
declare    FirstProfile=${TRUE}
# ------------------------------------ PROCEDURE ------------------------------------- #
fnPrint "${RUNSTARTED}"
# for each profile try and capture the output of the status
if fnGetStatusReports; then
    # read consolidated status reports to produce summary report
    for ReportLine in ${StatusReports[@]}; do
        if [[ "${ReportLine}" == "${CHAINSTARTTIME}"* ]]; then
            ChainStartTime="${ReportLine:18:10} ${ReportLine:38:4}"
        fi
        if [[ "${ReportLine}" =~ "${STARTOFPROFILE}" ]]; then
            if (( FirstProfile == TRUE )); then
                FirstProfile=${FALSE}
            else
                # print the profile total
                printf "\r${UP1}${WHITE}%70s\n" "============"
                printf "%70s\n\n" "$(numfmt --format="%'15.3f" \
                                            --to-unit=${SCALE}  \
                                              ${ProfileTotal})"
                # update the overall total
                GrandTotal=$((GrandTotal+ProfileTotal))
                # reset the profile total
                ProfileTotal=0
            fi
            fnPrintProfileHeader "${ReportLine}"
        elif [[ "${ReportLine}" =~ "${ARGS}" ]]; then
            # arguments line contains the path to the current prefix
            BackupsPath="${ReportLine##${ARGPREFIX}}"
            # for current prefix update the comment and backup size associative arrays
            for Manifest in $(find "${BackupsPath}" -name "*.manifest" -print); do
                fnUpdateHashes "${Manifest}"
            done
        elif [[ "${ReportLine}" =~ "${STARTOFBACKUPSET}" ]]; then
            # signal to start processing full or incremental detail lines
            Printing=${TRUE}
        else
            # if currently processing detail lines
            if (( Printing == TRUE )); then
                # end of backup set if not a full or incremental report line
                if [[ ! "${ReportLine}" =~ "${ISFULL}" && \
                      ! "${ReportLine}" =~ "${ISINCREMENT}" ]]; then
                    # print the backup set total
                    printf "${WHITE}%70s\n" "------------"
                    printf "%70s\n\n" "$(numfmt --format="%'15.3f" \
                                              --to-unit=${SCALE}  \
                                                 ${BackupTotal})"
                    # update the chain total
                    ChainTotal[${ChainStartTime}]=$(( ChainTotal[${ChainStartTime}]+BackupTotal ))
                    # update the profile total
                    ProfileTotal=$((ProfileTotal+BackupTotal))
                    # reset the backup set total
                    BackupTotal=0
                    # set flag to not processing a backup set
                    Printing=${FALSE}
                else
                    # get the size of the backup
                    Size="$(fnGetBackupSize "${ReportLine}")"
                    # add the size of the files in the backup to the detail line
                    ReportLine+="$(printf "%15s" "$(numfmt --format="%'13.3f" \
                                                           --to-unit=${SCALE}  \
                                                             ${Size})")"
                    # add the backup size to the backup set total
                    BackupTotal=$((BackupTotal+Size))
                    # add any description associated with the backup
                    ReportLine="$(fnAddDescription "${ReportLine}")"
                    # adjust the spacing between fields in the report line
                    ReportLine="$(fnFormatLine "${ReportLine}")"
                    # print the report line
                    if [[ "${ReportLine}" =~ "${ISFULL}" ]]; then
                        fnPrint "${WHITE}${ReportLine}${NOCOLOUR}${NL}"
                    else
                        fnPrint "${GRAY}${ReportLine}${NL}"
                    fi
                fi
            fi
        fi
    done
    # all profiles processed, output final report totals
    # print the final profile total
    printf "\r${UP1}${WHITE}%70s\n" "============"
    printf "%70s\n" "$(numfmt --format="%'15.3f" \
                                --to-unit=${SCALE}  \
                                  ${ProfileTotal})"
    # update the overall total
    GrandTotal=$((GrandTotal+ProfileTotal))
    # print the grand total
    printf "\n${YELLOW}%70s\n" "============"
    printf "%70s\n" "$(numfmt --format="%'15.3f" \
                              --to-unit=${SCALE}  \
                                ${GrandTotal})"
    printf "%70s\n" "============"
    # print the chain set totals
    GrandTotal=0
    fnPrint "${BOLDBLUE}\nChain Sets Totals\n=================\n${BOLDGREEN}"
    printf  "%42s %27s\n" "Run Date" "Size (Mb)"
    printf  "%42s %27s\n" "===============" "============"
    fnPrint "${WHITE}"
    # IFS=$'\n'
    for t in $(sort -M <<<$(printf "%s\n" "${!ChainTotal[*]}")); do
        printf "%42s %27s\n" "${t}" "$(numfmt --format="%'15.3f" \
                                              --to-unit=${SCALE}  \
                                              ${ChainTotal[${t}]})"
        GrandTotal=$(( GrandTotal+ChainTotal[${t}] ))
    done
    # unset IFS
    # print the grand total
    printf "${YELLOW}%70s\n" "============"
    printf "%70s\n" "$(numfmt --format="%'15.3f" \
                              --to-unit=${SCALE}  \
                                ${GrandTotal})"
    printf "%70s\n" "============"
    exit 0
else
    # something went wrong
    fnPrint "${PROFILEERROR}"
    fnPrint "${RUNFAILED}"
    exit 1
fi
