#!/bin/bash
# ==================================================================================== #
#         FILE: data-backup-summary.sh                                                 #
# ==================================================================================== #
#  DESCRIPTION: Runs duply to produce a status report for each profile directory found #
#               in the '.duply' directory (excluding 'example') and consolidates the   #
#               output into one large array. It then processess the array to create an #
#               overall summary report for all profiles found.                         #
# ==================================================================================== #
#        USAGE: data-backup-summary.sh (no arguments expected or accepted)             #
#        NEEDS: duply, grep, sed                                                       #
#    COPYRIGHT: Keith Watson (swillber@gmail.com)                                      #
#      VERSION: 1.1.0                                                                  #
# DATE CREATED: 07-Mar-2017                                                            #
# LAST CHANGED: 07-Aug-2021 15:26                                                      #
#   KNOWN BUGS: ---                                                                    #
# ==================================================================================== #
#  LICENSE:                                                                            #
#  This program is free software; you can redistribute it and/or modify it under the   #
#  terms of the GNU General Public License as published by the Free Software           #
#  Foundation; either version 2 of the License, or (at your option) any later version. #
#                                                                                      #
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY     #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A     #
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.           #
#                                                                                      #
#  You should have received a copy of the GNU General Public License along with this   #
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,   #
#  Fifth Floor, Boston, MA 02110=1301, USA.                                            #
#                                                                                      #
#  Copyright 2016 Keith Watson <swillber@gmail.com>                                    #
# ==================================================================================== #
#                                    F U N C T I O N S                                 #
# ==================================================================================== #
# ------------------------------------------------------------------------------------ #
function fnGetStatusReports () {
# ------------------------------------------------------------------------------------ #
# For each profile in the profiles list runs the duply status command and appends the  #
# output to the global StatusReports array.                                            #
# Arguments:                                                                           #
#   none                                                                               #
# Returns:                                                                             #
#   true (0) if all profiles found, false (1) if one or more profiles not found.       #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r DUPLYCMD="status"
    declare -r STDERR="/tmp/${SCRIPTNAME}${$}.wrk"
    declare -r PROFILESNOTFOUND="Profiles not found:"
    declare -r PROFILESFOUND="Profiles found:"
    # extract list of profile directories from the duply config directory
    # NB trailing slash on '.duply' directory means symbolic links are followed
    declare -a PROFILES=($(ls -1F ~/.duply/ | grep "/$" | grep -v example | sed "s/\///"))
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -a MissingProfiles
    declare -a FoundProfiles
    declare    Error=""
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    IFS=$'\n'                # delete field separators so that whole lines are not split
    for Profile in "${PROFILES[@]}"; do
        # append each duply command output to the array and capture stderr output
        mapfile -O ${#StatusReports[@]} StatusReports < \
                        <(duply "${Profile}" "${DUPLYCMD}" 2>"${STDERR}")
        Error=$(<"${STDERR}")  # get stderr output into variable
        if [[ -n ${Error} ]]; then
            MissingProfiles+=(" ${Profile}")
        else
            FoundProfiles+=(" ${Profile}")
        fi
    done
    if [[ ${#FoundProfiles[@]} > 0 ]]; then
        fnPrint "${NL}" "${PROFILESFOUND}" "${FoundProfiles[@]}" "${NL}"
    fi
    if [[ ${#MissingProfiles[@]} > 0 ]]; then
        fnPrint "${NL}" "${PROFILESNOTFOUND}" "${MissingProfiles[@]}" "${NL}"
        Result=${FALSE}
    fi
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnPrintDetail () {
# ------------------------------------------------------------------------------------ #
# Returns supplied arguments as a report detail line with each argument printed in a   #
# separate column. Columns are defined via the DETAILFORMAT constant passed as format  #
# string to a 'printf' command. The first invocation causes headings to be printed.    #
# Arguments: data to be printed.                                                       #
# Returns: no return value, outputs report line to stdout.                             #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r DETAILHEAD1="$(tr -d '\n' <<EOSL
Profile            Chain Type          Full Backup       Last Increment
   Sets  Volumes
EOSL
)"
    declare -r DETAILHEAD2="$(tr -d '\n' <<EOSL
------------ ----------------  -------------------  -------------------
   ----  -------
EOSL
)"
    declare -r DETAILFORMAT="%-12s %16s %20s %20s %6s %8s\n"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    DetailLine=""
    # ---------------------------------- PROCEDURE ----------------------------------- #
    if [[ ${FirstDetail} -eq ${TRUE} ]]; then
        fnPrint "${NL}${BOLDBLUE}${DETAILHEAD1}${NOCOLOUR}${NL}"
        fnPrint "${BOLDBLUE}${DETAILHEAD2}${NOCOLOUR}${NL}"
        FirstDetail=${FALSE}
    fi
    printf -v DetailLine "${DETAILFORMAT}" "${@}"
    fnPrint "${DetailLine}"
}
# ------------------------------------------------------------------------------------ #
function fnPrint () {
# ------------------------------------------------------------------------------------ #
# Output the arguments to stdout using the printf builtin.                             #
# Arguments: data to be displayed.                                                     #
# Returns: none.                                                                       #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    printf "%b" "${@}"
}
# ==================================================================================== #
#                                       M A I N                                        #
# ==================================================================================== #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
declare -ir TRUE=0
declare -ir FALSE=1
declare -r  NL=$'\n'
declare -r  SCRIPTNAME="${0##*/}"              # base name of script
# shellcheck disable=SC2034
# SC2034: 'variable' appears unused. Verify it or export it.
# ..............................console colour codes.................................. #
{ # code block so shellcheck directive applies to all declare statements
    declare -r BLACK="\e[0;30m"                # foreground colours
    declare -r RED="\e[0;31m"
    declare -r GREEN="\e[0;32m"
    declare -r BROWN="\e[0;33m"
    declare -r BLUE="\e[0;34m"
    declare -r PURPLE="\e[0;35m"
    declare -r CYAN="\e[0;36m"
    declare -r GRAY="\e[0;37m"
    declare -r DARKGRAY="\e[1;30m"             # bold foreground colours
    declare -r BOLDRED="\e[1;31m"
    declare -r BOLDGREEN="\e[1;32m"
    declare -r YELLOW="\e[1;33m"
    declare -r BOLDBLUE="\e[1;34m"
    declare -r BOLDPURPLE="\e[1;35m"
    declare -r BOLDCYAN="\e[1;36m"
    declare -r WHITE="\e[1;37m"
    declare -r NOCOLOUR="\e[0m"                # transparent - don't change
}
# ...........................runtime message texts.................................... #
declare -r REPORTNAME="Backup status summary report"
declare -r RUNSTARTED="\n${WHITE}=== ${REPORTNAME} started ===${NOCOLOUR}\n"
declare -r PROFILEERROR="\n${BOLDRED}### NB Some profiles not found ###${NOCOLOUR}\n"
declare -r RUNENDED="\n${WHITE}=== ${REPORTNAME} finished ===${NOCOLOUR}\n\n"
# ..............................status report line types.............................. #
# these are used to identify the report lines containing the data for the summary report
declare -r USINGPROFILE="Using profile"
declare -r SECONDARYCHAIN="Secondary chain "
declare -r PRIMARYCHAIN="Found primary backup chain"
declare -r CHAINSTART="Chain start time: "
declare -r CHAINEND="Chain end time: "
declare -r BACKUPSETS="Number of contained backup sets"
declare -r BACKUPVOLUMES="Total number of contained volumes"
# -------------------------------- GLOBAL VARIABLES ---------------------------------- #
declare -i FirstDetail=${TRUE}
declare -a StatusReports
# fields for each summary report line
declare    Profile=""
declare    Type=""
declare    Start=""
declare    End=""
declare    Sets=""
declare    Volumes=""
# ------------------------------------ PROCEDURE ------------------------------------- #
fnPrint "${RUNSTARTED}"
if fnGetStatusReports; then
    # read consolidated reports to produce summary report
    for ReportLine in "${StatusReports[@]}"; do
        ReportLine="${ReportLine%%$'\n'}"
        # only certain lines are of interest all others are just ignored
        if [[ ${ReportLine} =~ ${USINGPROFILE} ]]; then
            Profile="${ReportLine##U*/}"
            Profile="${Profile::-2}"
            fnPrint "${NL}"
        #
        elif [[ ${ReportLine} =~ ${SECONDARYCHAIN} ]]; then
            Type="${ReportLine#S*chain}"
            Type="Secondary${Type::-1}"
        #
        elif [[ ${ReportLine} =~ ${PRIMARYCHAIN} ]]; then
            Type="Primary"
        #
        elif [[ ${ReportLine} =~ ${CHAINSTART} ]]; then
            Start="${ReportLine#${CHAINSTART}}"
            Start=$(date -d "${Start}" +"%d/%m/%Y %X")
        #
        elif [[ ${ReportLine} =~ ${CHAINEND} ]]; then
            End="${ReportLine#${CHAINEND}}"
            End=$(date -d "${End}" +"%d/%m/%Y %X")
        #
        elif [[ ${ReportLine} =~ ${BACKUPSETS} ]]; then
            Sets="${ReportLine#N*: }"
        #
        elif [[ ${ReportLine} =~ ${BACKUPVOLUMES} ]]; then
            Volumes="${ReportLine#T*: }"
            fnPrintDetail "${Profile}" "${Type}" "${Start}" "${End}" "${Sets}" "${Volumes}"
            Profile=""
        fi
    done
else
    fnPrint "${PROFILEERROR}"
fi
fnPrint "${RUNENDED}"
