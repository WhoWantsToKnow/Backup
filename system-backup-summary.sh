#!/bin/bash
# ======================================================================================
#          FILE: system-backup-summary.sh
# ======================================================================================
#   DESCRIPTION: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
#                Integer nec odio. Praesent libero. Sed cursus ante dapibus
#                diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet.
#                Duis sagittis ipsum.
# ======================================================================================
#         USAGE: example.sh [-a] [-b] [-c argvalue] [-d] [argvalue]
#                (see ’fnUsage()’)
#         NEEDS: ---
#     COPYRIGHT: Keith Watson (swillber@gmail.com)
#       VERSION: 1.2.1
#  DATE CREATED: 07-Aug-2021 15:48
#  LAST CHANGED: 30-May-2023 21:16
#    KNOWN BUGS: ---
# ======================================================================================
#  LICENSE:
#  This program is free software; you can redistribute it and/or modify it under the
#  terms of the GNU General Public License as published by the Free Software
#  Foundation; either version 2 of the License, or (at my option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with this
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,
#  Fifth Floor, Boston, MA 02110-1301, USA.
# ======================================================================================
#                                   F U N C T I O N S                                  #
# ==================================================================================== #
# ------------------------------------------------------------------------------------ #
function fnCopies () {
# ------------------------------------------------------------------------------------ #
# Repeats a string value a specified number of times.                                  #
# Arguments:                                                                           #
#     1) string_expression - a string of one or more characters.                       #
#     2) integer - number of time string is to be repeated.                            #
# Returns:                                                                             #
#     a string.                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    C="${1}"
    declare -i N=${2}
    declare    S="$(printf "%*s" ${N} "")"
    declare    T="${S// /${C}}"
    # ---------------------------------- PROCEDURE ----------------------------------- #
    printf "%s" "${T}"
    return
}
# ------------------------------------------------------------------------------------ #
function fnBackupAvailable () {
# ------------------------------------------------------------------------------------ #
# check if the path defined by the argument is a mount point and a device is mounted.  #
# Arguments:                                                                           #
#   1) mountpoint of backup device                                                     #
# Returns:                                                                             #
#   True if found and false if not found                                               #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r PMOUNTS="/proc/mounts"
    declare -r MOUNT="${1}"
    declare -r MSG="Backup drive found on %s as %s.\n"
    declare -r ERR="ERROR: Backup drive not found. Please attach device and restart.\n"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result                       # used to return result of function
    declare    Message                      # holds message text
    declare    Mount                        # a single line from /proc/mounts
    declare -a Field                        # individual fields from $Mount
    # ---------------------------------- PROCEDURE ----------------------------------- #
    Result=${FALSE}
    while read -r Mount ; do
        if [[ ${Mount} =~ ${MOUNT} ]]; then
            Field=(${Mount})
            printf -v Message "${MSG}" "${Field[1]}" "${Field[0]}"
            Result=${TRUE}
            break
        fi
    done < "${PMOUNTS}"
    if [[ ${Result} -eq ${FALSE} ]]; then
        Message="${ERR}"
    fi
    printf "%s\n" "${Message}"
    # SC2086: Double quote to prevent globbing and word splitting.
    # shellcheck disable=SC2086
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnReportLine () {
# ------------------------------------------------------------------------------------ #
# output a line of session data.                                                       #
# Arguments:                                                                           #
#   none                                                                               #
# Returns:                                                                             #
#   none                                                                               #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    (( SessionNo++ ))
    if (( SetCount > 1 )); then
        SessionStatus="Complete"
    else
        SessionStatus="Incomplete"
    fi
    printf " %02d  %s  %s  %-12s  %3d %s   %s\n"                                        \
             ${SessionNo} "${BackupDate} ${BackupTime}" "${BackupType}"                \
            "${SessionStatus}" ${FilesInSession}                                       \
             $(numfmt --format="%'8.3fG" --to-unit=1000000000 ${SessionSize})          \
            "${BackupComment}"
    TotalFiles=$(( TotalFiles + FilesInSession ))
    FilesInSession=0
    TotalSize=$(( TotalSize + SessionSize ))
    SessionSize=0
    SetCount=0
    return
}
# ==================================================================================== #
#                                      M A I N                                         #
# ==================================================================================== #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
declare -ir TRUE=0
declare -ir FALSE=1
declare -r  SCRIPTNAME="${0##*/}"          # base name of script
declare -r  BACKUPDRIVE="/run/media/swillber/Backups"
declare -r  BACKUPSETROOT="*.fsa"
declare -r  RUNDATE="$(date +%Y%m%d)"
declare -r  SEPARATOR="$(fnCopies "~" 85)"
# --------------------------------- print messages ----------------------------------- #
declare -r PM1="%s started...\n"
declare -r PM2="%s ...finished OK.\n"
declare -r PM3="%s ...exiting after errors.\n"
declare -r PM4="\n\t\t\t  Total run time %s seconds.\n\n"
declare -r PM5="%s unique backup sets found, summarising by session:\n"
declare -r PM6="No unique backup sets found in ${BACKUPDRIVE}\n\n"
declare -r PM7=" No.    Date/Time       Type    Status    Files    Size    Comment\n"
declare -r PM8="\nN.B. To delete all backup sets before a given date use;\n"
declare -r PM9="\t$(tput sitm)touch -t yyyymmdd0000 [temp file spec]\n"
declare -r PM10="\tfind ${BACKUPDRIVE} -name "FSARCHIVE-SD*" ! -newer [temp file spec] -delete$(tput ritm)\n"
declare -r PM11="where dd is +1 day later than the given date.\n"
#                                                                                      #
# -------------------------------- GLOBAL VARIABLES ---------------------------------- #
declare -i Result=${TRUE}
declare -a BackupFilePaths
declare -i FileSize
declare -i BuSize
declare    SessionStatus
declare -i FilesInSession=0
declare -i SetCount=0
declare -i TotalFiles=0
declare -i TotalSize=0
declare -i SessionSize=0
declare -a ArchInfo
declare -a ArchLabel
declare    Comment
declare    OldDateTime=""
declare -i SessionNo=0
#                                                                                      #
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
printf "\n"
printf "${PM1}" "${SCRIPTNAME^}"        # indicate script started
# only continue if a backup drive is available
if fnBackupAvailable "${BACKUPDRIVE}"; then
    IFS=$'\n' # ignore spaces when processing filenames
    # get a list of all the root files of the backup sets on the backup drive
    # (backups can be multi-file)
    BackupFilePaths=($(find "${BACKUPDRIVE}" -name "${BACKUPSETROOT}" -print))
    # only produce report if files are present
    if [[ ${#BackupFilePaths[@]} -gt 0 ]]; then
        printf "${PM5}" "${#BackupFilePaths[@]}"
        printf "${PM7}${SEPARATOR}\n"

        # sort list into date order (date/time embedded in filename)
        readarray -td '' BackupFilePaths < <(printf '%s\0' "${BackupFilePaths[@]}" | sort -z -t- -k5)
        # process each backup root to get data:
        for BackupRoot in ${BackupFilePaths[*]}; do
            BackupFile="${BackupRoot%%.*}"  # remove extension from root file path
            # get date and time from backup filename
            BackupFilename="${BackupFile##*/}"     # remove path to get filename
            SessionDateTime="${BackupFilename##*-}" # get date/time from filename
            # see if new backup session started
            if ! [[ -z "${OldDateTime}" ]]; then   # only if not 1st time through
                 # new session started
                if [[ "${SessionDateTime}" != "${OldDateTime}" ]]; then
                    fnReportLine                   # output previous information
                fi
            fi
            OldDateTime="${SessionDateTime}" # save the session date/time
            # get formatted date
            BackupDate="${SessionDateTime::4}-${SessionDateTime:4:2}-${SessionDateTime:6:2}"
            # get formatted time
            BackupTime="${SessionDateTime:8:2}:${SessionDateTime:10:2}"
            # get backup type
            BackupType="${BackupFile%/*}"   # remove filename to give root path
            BackupType="${BackupType##*/}"  # remove all but last part of root path
            # get archive information into an array
            mapfile -t ArchInfo < <(fsarchiver archinfo "${BackupRoot}" 2>&1)
            # extract backup comment from report
            for Line in "${ArchInfo[@]}"; do
                case "${Line}" in
                    #   Archive label:    <none>
                    "Archive label:"*)
                        BackupComment="${Line:18}"
                        ;;
                esac
            done
            # 4) for all files in backup set:
            # count number of files in backup set
            FilesInSet=$(find "${BackupFile%/*}" -name "${BackupFile##*/}*" -print | wc -l)
            # accumulate number of files in session
            FilesInSession=$(( FilesInSession + FilesInSet ))
            # count number of bytes of all files in set
            SizeOfBackupSet="$(du -bc ${BackupFile}* | tail -n1)"
            # remove text from end of number
            SizeOfBackupSet="${SizeOfBackupSet//[^0-9]/}"
            # accumulate total size of all files in session
            SessionSize=$(( SessionSize + SizeOfBackupSet ))
            # count set in session
            (( SetCount++ ))
        done
        fnReportLine                         # output final session information
    else
        printf "${PM6}"
    fi
fi
printf "\t\t\t\t\t     ===  ========\n"
printf "%48d%s\n" ${TotalFiles} $(numfmt --format="%'9.3fG" --to-unit=1000000000 ${TotalSize})
printf "${PM8}"
printf "${PM9}"
printf "${PM10}"
printf "${PM11}"
exit
