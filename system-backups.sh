#!/bin/bash
# ==================================================================================== #
#         FILE: system-backups.sh                                                      #
# ==================================================================================== #
#  DESCRIPTION: This script uses fsarchiver (http://www.fsarchiver.org) to backup one  #
#               or more unmounted partitions to another partition (usually a           #
#               removable drive). It will also save the partition table of each disk   #
#               where a partition is backed up from.                                   #
#                                                                                      #
#               NB must be run with root privileges i.e. use sudo                      #
#                                                                                      #
# ==================================================================================== #
#        USAGE: see fnUsage                                                            #
#        NEEDS: fsarchiver,lsblk,sgdisk,7zip                                           #
#    COPYRIGHT: Keith Watson (swillber@gmail.com)                                      #
#      VERSION: 1.6.3                                                                  #
# DATE CREATED: 04-Nov-2016                                                            #
# LAST CHANGED: 29-Jun-2024 18:30                                                      #
#   KNOWN BUGS: ---                                                                    #
# ==================================================================================== #
#  LICENSE:                                                                            #
#  This program is free software; you can redistribute it and/or modify it under the   #
#  terms of the GNU General Public License as published by the Free Software           #
#  Foundation; either version 2 of the License, or (at your option) any later version. #
#                                                                                      #
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY     #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A     #
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.           #
#                                                                                      #
#  You should have received a copy of the GNU General Public License along with this   #
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,   #
#  Fifth Floor, Boston, MA 02110=1301, USA.                                            #
#                                                                                      #
#  Copyright 2016 Keith Watson <swillber@gmail.com>                                    #
# ==================================================================================== #
#                                   F U N C T I O N S                                  #
# ==================================================================================== #
# ------------------------------------------------------------------------------------ #
function fnUsage () {
# ------------------------------------------------------------------------------------ #
# Display usage message.                                                               #
# Arguments: none.                                                                     #
# Returns: none.                                                                       #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r USAGE=$(cat <<EOSL

Usage:
    ${SCRIPTNAME} -[i|I] Partitions -[o|O] BackMount [-[a|A] BuDir]
                 [-[p|P] BuPTbl]   [-[l|L] LogDir]   [-[d|D] Description]  [-[h|H]]
    Backup filesystems using fsarchiver and also save partition table.
    where;
            Partitions  = list of devices to be backed up, enclosed in quotes or
                          parentheses. Format can be '/dev/sdxn' or 'sdxn'. Partitions
                          should not be currently mounted. (M)
            BackMount   = mount point where backup images are to be written. (M)
                          (usually mount point of a removable device).
            BuDir       = path under 'BackMount' where file system images are to be
                          written (default is '/System/Archives').
            BuPTbl      = path under 'BackMount' where partition table data will be
                          written (default is '/System/PartitionTable'). If '-a' is
                          provided but not '-p' then that path is used for both.
            LogDir      = path under 'BackMount' where runtime logs are to be
                          written (default '/System/RunLogs')
            Description = descriptive text to be stored along with backup file(s)
                          - truncated to 35 characters (default 'Routine Weekly Backup')
            -h          = displays this help text.

    NOTES:
        1) requires root privileges, e.g. use sudo.
        2) options are not case sensitive, uppercase will be converted to lowercase.
        3) (M) indicates mandatory option.

EOSL
)
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnEcho "${USAGE}${NL}${NL}"
}
# ------------------------------------------------------------------------------------ #
function fnGetOpts () {
# ------------------------------------------------------------------------------------ #
# Get command line options (see fnUsage)                                               #
# Sets Partitions  = command line option -i|-I                                         #
#      BackMount   = command line option -o|-O                                         #
#      BuDir       = command line option -a|-A                                         #
#      BuPTbl      = command line option -p|-P                                         #
#      LogDir      = command line option -l|-L                                         #
#      Description = command line option -d|-D                                         #
# Prints a usage message if any errors found.                                          #
# Arguments: All the command line options passed as $@.                                #
# Returns: true if options found ok otherwise false.                                   #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r OPTIONS=":i:I:o:O:a:A:p:P:l:L:d:D:hH"+
    declare -r BUDIR="/System/Archives"         # default value
    declare -r BUPTBL="/System/PartitionTable"  # default value
    declare -r LOGDIR="/System/RunLogs"         # default value
    declare -r COMMENT="Routine Weekly Backup"  # default value
    # --------------------------- Error Message Templates ---------------------------- #
    declare -r ERR1="ERROR: Missing argument: Option '-%s', must have an argument.\n"
    declare -r ERR2="ERROR: Not recognized: Option '-%s' is unknown.\n"
    declare -r ERR3="ERROR: %s is not a valid directory path.\n"
    declare -r ERR4="ERROR: Could not create '%s', is it a valid path?\n"
    declare -r ERR5="ERROR: Logfile comment '%s' contains control characters\n"
    declare -r ERM6="\nFATAL ERROR: must be run with root privileges.\n"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare Char1=""
    declare Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    if [[ ${#} -gt 0 ]]; then
        while getopts "${OPTIONS}" OPT; do
            OPT="${OPT,,}"                  # ensure lowercase
            Char1="${OPTARG:0:1}"           # in case of a missing argument to an option
            if [[ ${Char1} == "-" ]]; then
                printf "${ERR1}" "${OPT}"
                Result=${FALSE}
                break
            fi
            case ${OPT} in
                i)  # list of partitions to be backed up
                    fnSetParts "${OPTARG}"  # populates $Partitions if valid
                    if [[ -z ${Partitions} ]]; then
                        Result=${FALSE}
                        break
                    fi
                    ;;
                o)  # backup device (or partition) mount point
                    if [[ -d ${OPTARG} ]]; then
                        BackMount="${OPTARG}"
                    else
                        printf "${ERR3}" "${OPTARG}"
                        Result=${FALSE}
                        break
                    fi
                    ;;
                a)  # where fsarchiver files will be saved under the mount point
                    BuDir="${OPTARG}"
                    ;;
                p)  # where partition table file will be saved under mount point
                    BuPTbl="${OPTARG}"
                    ;;
                l)  # where runtime logs will be saved under mount point
                    LogDir="${OPTARG}"
                    ;;
                d)  # descriptive text to be saved along with backup file(s)
                    # (truncated to 35 characters)
                    Comment="${OPTARG::35}"
                    ;;
                h)  # display help text
                    Result=${FALSE}
                    break
                    ;;
                :)  # missing argument to option
                    printf "${ERR1}" "${OPTARG}"
                    Result=${FALSE}
                    break
                    ;;
                \?) # invalid option
                    printf "${ERR2}" "${OPTARG}"
                    Result=${FALSE}
                    break
                    ;;
            esac
        done
    else
        Result=${FALSE}
    fi
    # -------------------------------------------------------------------------------- #
    # Validate supplied values and set defaults.                                       #
    # -------------------------------------------------------------------------------- #
    if [[ ${Result} -eq ${TRUE} ]]; then       # if no errors so far
        if [[ $USER != 'root' ]]; then         # if user not 'root' exit with error
            fnEcho "${ERM6}"
            Result=${FALSE}
        fi
    fi
    if [[ ${Result} -eq ${TRUE} ]]; then       # if no errors so far
        # so check which of $BuDir and/or $BuPTbl have been provided and also if they
        # are valid.
        if [[ -z "${BuPTbl}" ]]; then
            # partition table location not specified
            if [[ -z "${BuDir}" ]]; then
                # both empty so set both defaults
                BuDir="${BUDIR}"
                BuPTbl="${BUPTBL}"
            else
                # only filesystem backup location specified so set partition table
                # location to it.
                BuPTbl="${BuDir}"
            fi
        else
            # partition table location is specified
            if [[ -z "${BuDir}" ]]; then
                BuDir="${BUDIR}"            # set default filesystem backup location
            fi
        fi
        # create if doesn't exist (also checks if paths are valid)
        BuDir="${BackMount}${BuDir}"
        BuPTbl="${BackMount}${BuPTbl}"
        if sudo -u "${SUDO_USER}" mkdir -p "${BuDir}"; then
            if sudo -u "${SUDO_USER}" mkdir -p "${BuPTbl}"; then
                : # no-op
            else
                printf "${ERR4}" "${BuPTbl}"
                Result=${FALSE}
            fi
        else
            printf "${ERR4}" "${BuDir}"
            Result=${FALSE}
        fi
    fi
    if [[ ${Result} -eq ${TRUE} ]]; then  # if no errors so far
        # check if $LogDir has been provided and also if it is valid.
        if [[ -z "${LogDir}" ]]; then
            LogDir="${LOGDIR}"            # set default runtime logfile location
        fi
        # create if doesn't exist (also checks if paths are valid)
        LogDir="${BackMount}${LogDir}"
        if sudo -u "${SUDO_USER}" mkdir -p "${LogDir}"; then
            : # no-op
        else
            printf "${ERR4}" "${LogDir}"
            Result=${FALSE}
        fi
    fi
    if [[ ${Result} -eq ${TRUE} ]]; then  # if no errors so far
        # set default if $Comment has not been provided.
        if [[ -z "${Comment}" ]]; then
            Comment="${COMMENT}"          # set default logfile comment
        fi
        # check text has no control characters using POSIX class :print: (visible
        # characters and spaces [i.e. anything except control characters])
        if ! [[ "${Comment}" =~ [^:print:] ]]; then
            printf "${ERR5}" "${Comment}"
            Result=${FALSE}
        fi
    fi
    return "${Result}"
}
# ------------------------------------------------------------------------------------ #
function fnSetParts () {
# ------------------------------------------------------------------------------------ #
# Takes a list of possible partition ids and checks that they are valid partitions.    #
# Partition ids can take the form '/dev/sdxn' or just 'sdxn'. If any partition is      #
# mounted the script is immediately exited.                                            #
# Arguments:                                                                           #
#       1) list of partition ids.                                                      #
# Returns:                                                                             #
#       sets global array $Partitions if all ids are valid, otherwise leaves array     #
#       unset.                                                                         #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -ar PARTITIONS=(${@})
    declare -r  ERR1="ERROR: partition %s not found.\n"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -a Result
    # ---------------------------------- PROCEDURE ----------------------------------- #
    for Partition in "${PARTITIONS[@]}"; do
        Partition="${Partition,,}"       # convert to lowercase
        Partition="${Partition#/dev/}"   # remove '/dev/' prefix if present
        Partition="/dev/${Partition}"    # then put it back - stops /dev/dev/...
        if [[ $(lsblk -ldno type "${Partition}" 2>/dev/null) =~ ^part ]]; then
            Result+=("${Partition}")
        else
            printf "${ERR1}" "${Partition}"
            unset -v Result
            break
        fi
    done
    if fnPartitionsMounted "${Result[@]}"; then
        # SC2086: Double quote to prevent globbing and word splitting.
        # shellcheck disable=SC2086
        exit ${FALSE}
    fi
    Partitions=(${Result[@]})
}
# ------------------------------------------------------------------------------------ #
function fnPartitionsMounted () {
# ------------------------------------------------------------------------------------ #
# Scan '/proc/mounts' to check if any of the device ids supplied are mounted.          #
# Arguments: 1) device ids ('/dev/xxxn') to be checked.                                #
# Returns:   True if device mounted else returns false.                                #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r PMOUNTS="/proc/mounts"
    declare -r MSG="ERROR: Device %s is mounted on %s. Please unmount.\n"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result
    declare    Mount                        # a single line from /proc/mounts
    declare -a Field                        # individual fields from $Mount
    # ---------------------------------- PROCEDURE ----------------------------------- #
    Result=${FALSE}
    for Device in "${@}"; do
        while read -r Mount ; do
            if [[ ${Mount} =~ ${Device} ]]; then
                Field=(${Mount})
                printf "${MSG}" "${Field[0]}" "${Field[1]}"
                Result=${TRUE}
            fi
        done < "${PMOUNTS}"
    done
    # SC2086: Double quote to prevent globbing and word splitting.
    # shellcheck disable=SC2086
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnBackupMounted () {
# ------------------------------------------------------------------------------------ #
# check if the path defined by the argument is a mount point and a device is mounted.  #
# Arguments:                                                                           #
#   1) mountpoint of backup device                                                     #
# Returns:                                                                             #
#   True if found and false if not found                                               #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r PMOUNTS="/proc/mounts"
    declare -r MOUNT="${1}"
    declare -r MSG="Backup drive found on %s as %s.\n"
    declare -r ERR="ERROR: Backup drive not found. Please attach device and restart.\n"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result                       # used to return result of function
    declare    Message                      # holds message text
    declare    Mount                        # a single line from /proc/mounts
    declare -a Field                        # individual fields from $Mount
    # ---------------------------------- PROCEDURE ----------------------------------- #
    Result=${FALSE}
    while read -r Mount ; do
        if [[ ${Mount} =~ ${MOUNT} ]]; then
            Field=(${Mount})
            printf -v Message "${MSG}" "${Field[1]}" "${Field[0]}"
            Result=${TRUE}
            break
        fi
    done < "${PMOUNTS}"
    if [[ ${Result} -eq ${FALSE} ]]; then
        Message="${ERR}"
    fi
    fnEcho "${Message}"
    # SC2086: Double quote to prevent globbing and word splitting.
    # shellcheck disable=SC2086
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnSavePartTable () {
# ------------------------------------------------------------------------------------ #
# Use 'sgdisk' to save the partition table of a specified disk to a nominated          #
# directory.                                                                           #
# Arguments:                                                                           #
#   1) device address of disk being backed up.                                         #
#   2) path to directory where partition table backup is to be stored.                 #
# Returns:                                                                             #
#   Return value from sgdisk backup operation.                                         #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r DEVADDR="${1}"
    declare -r DEVICE="${DEVADDR#/dev/}"
    declare -r BUPTBL="${2}"
    declare -r GPTFILE="Device-${DEVICE^^}-Partition-Table-$(fnTS).gpt"
    declare -r MSG1="\nTrying to save partition table of %s to %s\n"
    declare -r MSG2="Partition table not saved.\n"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    printf "${MSG1}" "${DEVADDR}" "${BUPTBL}/${GPTFILE}"
    sgdisk -b "${BUPTBL}/${GPTFILE}" "${DEVADDR}"
    Result=${?}
    if [[ ${Result} -ne ${TRUE} ]]; then
        fnEcho "${MSG2}"
    fi
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnArchiveLogFiles () {
# ------------------------------------------------------------------------------------ #
# Adds any run logs for the current filesystem to a 7z archive file and, if successful,#
# deletes the run log files. Intended to be invoked prior to filesystem backup.        #
# Arguments: none                                                                      #
# Returns: nothing                                                                     #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r ARCFILE="${LogDir}/${Label}.${RUNLOG}s.7z"
    declare -r LOGPATTERN="${Label}.*.${RUNLOG}"
    declare -r RUNLOGS="${LogDir}/${LOGPATTERN}"
    declare -r ACTIVELOG="$*"
    declare -r ARCHIVER="7z u -bso0 -bse1 -x!${ACTIVELOG}"
    declare -r MSG1="Updating archive file: %s\n"
    declare -r MSG2="Update failed\n"
    declare -r MSG3="Skip log file: %s\n"
    declare -r MSG4=" Add log file: %s\n"
    # ---------------------------------- VARIABLES ----------------------------------- #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnEcho "\n"
    printf "${MSG1}" "${ARCFILE}"
    ${ARCHIVER} "${ARCFILE}" "${RUNLOGS}"   # update archive with new run logs
    if [[ $? -lt 1 ]]; then                 # if update ok then delete run logs
        for LogFile in $(find ${LogDir} -maxdepth 1 -name "${LOGPATTERN}" -print); do
            if [[ "${ACTIVELOG}" == "${LogFile}" ]]; then
                printf "${MSG3}" "$(basename ${LogFile})"
            else
                rm -f "${LogFile}"
                printf "${MSG4}" "$(basename ${LogFile})"
            fi
        done
    else
        fnEcho "${MSG2}"
    fi
    fnEcho "\n"
    return
}
# ------------------------------------------------------------------------------------ #
function fnSaveFilesystem () {
# ------------------------------------------------------------------------------------ #
# Use 'fsarchiver' to backup a specified partition.                                    #
# Arguments: 1) device address of partition to be backed up.                           #
# Returns: True if fsarchiver finished with no errors, otherwise false.                #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r LEVEL="19"
    declare -r CORES="8"
    declare -r MEGABYTES="1025"
    declare -r DEVADDR="${1}"
    declare -r SAVEFILESYSTEM="savefs"
    declare -r VERBOSE="--verbose"
    declare -r OVERWRITE="--overwrite"
    declare -r COMPRESS="--zstd"
    declare -r THREADS="--jobs"
    declare -r SPLIT="--split"
    declare -r LABEL="--label"
    declare -r EXCLUDE="--exclude=.cache"
    declare -r PARTITION="${DEVADDR#/dev/}"
    declare -r ARCFILE="FSARCHIVE-${PARTITION^^}-${Label}-${LEVEL}-${RUNDATIME}.fsa"
    declare -r LOGFILE="${LogDir}/${Label}.$(fnTS).${RUNLOG}"
    declare -r PROGRESS="$(fnCopies "#" 100)"
    declare -r BAR="$(fnCopies "-" 100)"
    declare -r MSG1="Archiving ${DEVADDR} - ${Label} to ${ARCFILE}\n"
    declare -r MSG2="Filesystem backup completed OK.\n"
    declare -r MSG3="### Filesystem backup failed.\nn"
    declare -r MSG4="Filesystem backup took %s.\n"
    declare -r MSG6="Starting backup...\n"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i ArcStart
    declare -i ArcEnd
    declare -i ArcTime
    declare    ArcLine
    declare    Done=0
    declare -i Elapsed=0
    declare -i Runtime=0
    declare -i Timeleft=0
    declare    Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnEcho "\n${MSG1}"
    fnEcho "$(date +"%X %x") ${MSG1}" >> "${LOGFILE}"
    fnArchiveLogFiles "${LOGFILE}" >> "${LOGFILE}"
    fnEcho "${SEPARATOR}\n"
    ArcStart=$(date +"%s")
    # process console lines from fsarchiver and filter out progress lines to display
    # progress bar on console. Other lines are written to a run log file.
    # NB requires verbose parameter.
    while IFS= read -r ArcLine; do
        if [[ ${ArcLine:0:1} == "-" ]]; then
            ArcTime=$(date +"%s")
            Elapsed=$(( ArcTime - ArcStart ))
            Done=${ArcLine:6:3}                     # extract percentage complete
            if (( Done == 0 ));then                 # avoid division by zero errors
                Runtime=0
                Timeleft=0
            else                                    # crudely calculate runtime from %
                Runtime=$(( Elapsed * 100 / Done )) # complete and elapsed time so far.
                Timeleft=$(( Runtime - Elapsed ))
            fi
            # overwrite current line with progress bar
            ArcLine="Elapsed:$(fnHMS ${Elapsed}) ${Done}% "     # elapsed time
            ArcLine+="[${PROGRESS:0:${Done}}${BAR:${Done}}] "   # progress bar
            ArcLine+="To go:$(fnHMS ${Timeleft}) "              # time to finish
            ArcLine+="Final:$(fnHMS ${Runtime})"                # total run time
            # print composite progress bar
            fnEcho "\r${ArcLine}"               # carriage return but no line feed
        else
            fnEcho "${ArcLine}\n" >> "${LOGFILE}"   # write to log file
        fi
    done < <(fsarchiver "${SAVEFILESYSTEM}"\
                        "${OVERWRITE}"\
                        "${COMPRESS}=${LEVEL}"\
                        "${THREADS}=${CORES}"\
                        "${SPLIT}=${MEGABYTES}"\
                        "${BuDir}/${ARCFILE}"\
                        "${DEVADDR}"\
                        "${VERBOSE}"\
                        "${LABEL}=\"${Comment}\""\
                        "${EXCLUDE}"\
                        2>&1 \
                    )
    Result=${?}
    fnEcho "\n${SEPARATOR}\n"
    ArcEnd=$(date +"%s")
    if [[ $Result -eq ${TRUE} ]]; then
        printf "${MSG2}"
        printf "$(date +"%X %x") ${MSG2}" >>  "${LOGFILE}"
        ArcTime=$(( ArcEnd - ArcStart))
        fnEcho "$(printf "${MSG4}" "$(fnRT ${ArcTime})")"
        fnEcho "$(date +"%X %x") $(printf "${MSG4}" "$(fnRT ${ArcTime})")" >>  "${LOGFILE}"
    else
        printf "${MSG3}"
    fi
    fnEcho "${NL}"
    # SC2086: Double quote to prevent globbing and word splitting.
    # shellcheck disable=SC2086
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnCopies () {
# ------------------------------------------------------------------------------------ #
# Repeats a string value a specified number of times.                                  #
# Arguments:                                                                           #
#     1) string_expression - a string of one or more characters.                       #
#     2) integer - number of time string is to be repeated.                            #
# Returns:                                                                             #
#     a string.                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    C="${1}"
    declare -i N=${2}
    declare    S="$(printf "%*s" ${N} "")"
    declare    T="${S// /${C}}"
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnEcho "${T}"
    return
}
# ------------------------------------------------------------------------------------ #
function fnGetDrive () {
# ------------------------------------------------------------------------------------ #
# Search the backup target for a directory of the form '£DriveX' and, if found, return #
# the direcotry name. If not found returns an empty string.                            #
# Arguments: none                                                                      #
# Returns: see above                                                                   #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    Drive=""
    declare -i Result=0
    # ---------------------------------- PROCEDURE ----------------------------------- #
    Drive="$(find "${BackMount}" -type d -name "£Drive*" -print)"
    if [[ ${#Drive} -gt 0 ]]; then      # was it found? (look for zero length string)
        Drive="${Drive##${BackMount}}"  # strip off mount path
    else
        Drive=""
    fi
    fnEcho "${Drive#/£}"
    # SC2086: Double quote to prevent globbing and word splitting.
    # shellcheck disable=SC2086
    return ${Result}
}
function fnRT () {
# ------------------------------------------------------------------------------------ #
# Takes a single integer argument and converts it to a string of the form;             #
#  "[HH hrs ][MM mins ]SS secs"                                                        #
# Arguments:                                                                           #
#     1) integer seconds.                                                              #
# Returns:                                                                             #
#     String as described above.                                                       #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -ir SECONDS=${1}
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i  Remainder
    declare -i  Seconds
    declare -i  Minutes
    declare -i  Hours
    declare     Runtime=""
    # ---------------------------------- PROCEDURE ----------------------------------- #
    Hours=$(( SECONDS / 3600 ))
    Remainder=$(( SECONDS % 3600 ))
    Minutes=$(( Remainder / 60 ))
    Seconds=$(( Remainder % 60 ))
    if (( Hours > 0 )); then
        Runtime=$(printf "%d hrs " "${Hours}")
        Runtime="${Runtime}$(printf "%d mins " "${Minutes}")"
    else
        if (( Minutes > 0 )); then
            Runtime="$(printf "%d mins " "${Minutes}")"
        fi
    fi
    Runtime="${Runtime}$(printf "%d secs" "${Seconds}")"
    printf "%s" "${Runtime}"
}
# ------------------------------------------------------------------------------------ #
function fnHMS () {
# ------------------------------------------------------------------------------------ #
# Takes a single integer argument and converts it to a string of the form; "HH.MM.SS"  #
# Arguments:                                                                           #
#     1) integer seconds.                                                              #
# Returns:                                                                             #
#     String as described above.                                                       #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -ir SECONDS=${1}
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i  Remainder
    declare -i  Seconds
    declare -i  Minutes
    declare -i  Hours
    declare     Runtime=""
    # ---------------------------------- PROCEDURE ----------------------------------- #
    Hours=$(( SECONDS / 3600 ))
    Remainder=$(( SECONDS % 3600 ))
    Minutes=$(( Remainder / 60 ))
    Seconds=$(( Remainder % 60 ))
    Runtime="$(printf "%02d.%02d.%02d" "${Hours}" "${Minutes}" "${Seconds}")"
    printf "%s" "${Runtime}"
}
# ------------------------------------------------------------------------------------ #
function fnTS () {
# ------------------------------------------------------------------------------------ #
# Print the current date and time in the format yyyymmddhhmmss                         #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r TS="%Y%m%d%H%M%S"
    # ---------------------------------- VARIABLES ----------------------------------- #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    date +"${TS}"
}
# ------------------------------------------------------------------------------------ #
function fnDT () {
# ------------------------------------------------------------------------------------ #
# Print the current date and time in the format 'dayname dd-mmm-yy hh:mm:ss timezone'  #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r DT="%a %d-%b-%y %H:%M:%S %Z"
    # ---------------------------------- VARIABLES ----------------------------------- #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    date +"${DT}"
}
# ------------------------------------------------------------------------------------ #
function fnEcho () {
# ------------------------------------------------------------------------------------ #
# output the arguments using the printf builtin.                                       #
# Arguments:                                                                           #
#   1) data to be displayed                                                            #
# Returns:                                                                             #
#   none                                                                               #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    printf "%b" "${@}"
}
# ------------------------------------------------------------------------------------ #
# ==================================================================================== #
#                                      M A I N                                         #
# ==================================================================================== #
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
declare -ir TRUE=0
declare -ir FALSE=1
declare -r  NL=$'\n'
declare -r  SCRIPTNAME="${0##*/}"          # base name of script
declare -r  RUNDATE="$(date +%Y%m%d)"
declare -r  RUNDATIME="${RUNDATE}$(date +%H%M)"
declare -r  SEPARATOR="$(fnCopies "~" 80)"
declare -r  RUNLOG="run.log"
declare -r  LOGPATH="$(cat /tmp/backuplog.path)"
# --------------------------------- print messages ----------------------------------- #
declare -r PM1="\n%s started...\n\n"
declare -r PM2="Writing partition table data to %s\n"
declare -r PM3="Writing filesystem archives to %s\n"
declare -r PM4="%s ...finished OK.\n"
declare -r PM5="%s ...exiting after errors.\n"
declare -r PM6="Total run time %s\n\n"
declare -r PM7="Writing run logs to %s\n"
declare -r PM8="%s %s %s %s\n"
declare -r PM9="Backup file description:%s\n"
# --------------------------------- error messages ----------------------------------- #
declare -r ERM2="FATAL ERROR: could not save partition table for %s\n"
# -------------------------------- GLOBAL VARIABLES ---------------------------------- #
# ------------------------------- set by fnGetOpts() --------------------------------- #
declare -a Partitions             # list of partitions to be backed up
declare    BackMount              # mountpoint for device to receive backups
declare    BuDir                  # path under 'BackMount' where file system images are
                                  # to be written (default '/System/Archives')
declare    BuPTbl                 # path under 'BackMount' where partition table data
                                  # will be written (default '/System/PartitionTable')
declare    LogDir                 # path under 'BackMount' where runtime logs are to be
                                  # written (default '/System/RunLogs')
declare    Comment                # descriptive comment to be saved with backup file(s)
# ------------------------------------------------------------------------------------ #
declare -i Result=${TRUE}
declare    SrcDev=""                     # device we're going to backup from
declare    WrkDev=""
declare    Label=""
declare -i RunStart
declare -i RunEnd
declare -i RunTime
declare    ArcFile=""
declare -i ArcFileSize=0
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
if fnGetOpts "${@}"; then                # process command line arguments
    if fnBackupMounted "${BackMount}"; then
        fnEcho "${NL}"
        RunStart=$(date +"%s")
        printf "${PM1}" "${SCRIPTNAME^}"
        printf "${PM2}" "${BuPTbl}"
        printf "${PM3}" "${BuDir}"
        printf "${PM9}" "${Comment}"
        printf "${PM7}" "${LogDir}"
        # ------------------------------------------------------------------------ #
        # process partitions one at a time.                                        #
        # ------------------------------------------------------------------------ #
        for Partition in "${Partitions[@]}"; do
            Label="$(lsblk -no label "${Partition}")"
            # get the base device address, i.e. remove partition number
            # is partition ref of type /dev/sdxn or /dev/nvmexxxpn? test length
            if [[ "${#Partition}" -gt 9 ]]; then
                WrkDev="${Partition::-2}"   # /dev/nvmexxxpn --> /dev/nvmexxx
            else
                WrkDev="${Partition::-1}"   # /dev/sdxn --> /dev/sdx
            fi
            if [[ "${SrcDev}" != "${WrkDev}" ]]; then
                if fnSavePartTable "${WrkDev}" "${BuPTbl}"; then  # change of device
                    SrcDev="${WrkDev}"
                else
                    printf "${ERM2}" "${WrkDev}"    # oops, didn't work so bail out
                    Result=${FALSE}
                    break
                fi
            fi
            fnSaveFilesystem "${Partition}"
            Result=${?}
            if [[ ${Result} -ne ${TRUE} ]]; then
                break                   # if fsarchiver errors, exit loop
            fi
        done
        chown -R "${SUDO_USER}" "${BuPTbl}"
        chown -R "${SUDO_USER}" "${BuDir}"
        if [[ ${Result} -eq ${TRUE} ]]; then
            printf "${PM4}" "${SCRIPTNAME^}"
        else
            printf "${PM5}" "${SCRIPTNAME^}"
        fi
        RunEnd=$(date +"%s")
        RunTime=$(( RunEnd - RunStart))
        printf "${PM6}" "$(fnRT ${RunTime})"
    fi
    # add entry to backup log
    if [[ ${Result} -eq ${TRUE} ]]; then
        printf "${PM8}" "$(fnDT)" "$(fnGetDrive)" "OK" "$(basename $0) $*" \
                                                                >> "${LOGPATH}"
    else
        printf "${PM8}" "$(fnDT)" "$(fnGetDrive)" "Failed" "$(basename $0) $*" \
                                                                >> "${LOGPATH}"
    fi
else
    fnUsage
    Result=${FALSE}
fi
exit ${Result}
