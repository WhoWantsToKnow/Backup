#!/bin/bash
if [[ $USER != 'root' ]]; then
    printf "%b" "FATAL ERROR: must be run with root privileges.\n"
    exit 1
fi
#
# arg1 = restore to device (/dev/sdx)
# arg2 = partition table backup file
# arg3 = boot partition backup file
# arg4 = root partition backup file
# arg5 = home partition backup file
#
set -x
#
sgdisk --load-backup=${2} ${1}
#
fsarchiver -v -j7 -e '/.Trash-1000' restfs ${3} "id=0,dest="${1}"1" #>boot-restore.log 2>&1
#
fsarchiver -v -j7 -e '/tmp/*' -e '/var/tmp/*' -e '/var/cache/pacman/pkg/*' restfs ${4} "id=0,dest="${1}"2" #>root-restore.log 2>&1
#
fsarchiver -v -j7 restfs ${5} "id=0,dest="${1}"3" #>home-restore.log 2>&1
