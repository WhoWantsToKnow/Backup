#!/bin/bash
# ==================================================================================== #
#         FILE: data-backups.sh                                                        #
# ==================================================================================== #
#  DESCRIPTION: Runs duply for each of a predefined list of backup profiles. Any       #
#               arguments are saved as a comment to be saved with the backup.          #
# ==================================================================================== #
#        USAGE: no arguments                                                           #
#        NEEDS: duply                                                                  #
#    COPYRIGHT: Keith Watson (swillber@gmail.com)                                      #
#      VERSION: 2.1.0                                                                  #
# DATE CREATED: 04-Nov-2016                                                            #
# LAST CHANGED: 29-Nov-2021 23:39                                                      #
#   KNOWN BUGS: ---                                                                    #
# ==================================================================================== #
#  LICENSE:                                                                            #
#  This program is free software; you can redistribute it and/or modify it under the   #
#  terms of the GNU General Public License as published by the Free Software           #
#  Foundation; either version 2 of the License, or (at your option) any later version. #
#                                                                                      #
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY     #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A     #
#  PARTICULAR PURPOSE.  See the GNU General Public License for more details.           #
#                                                                                      #
#  You should have received a copy of the GNU General Public License along with this   #
#  program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street,   #
#  Fifth Floor, Boston, MA 02110=1301, USA.                                            #
#                                                                                      #
#  Copyright 2016 Keith Watson <swillber@gmail.com>                                    #
# ==================================================================================== #
#                                   F U N C T I O N S                                  #
# ==================================================================================== #
# ------------------------------------------------------------------------------------ #
function fnTS () {
# ------------------------------------------------------------------------------------ #
# Print the current date and time in the format yyyymmddhhmmss                         #
# Arguments: none                                                                      #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r TS="%Y%m%d%H%M%S"
    # ---------------------------------- VARIABLES ----------------------------------- #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    date +"${TS}"
}
# ------------------------------------------------------------------------------------ #
function fnHMS () {
# ------------------------------------------------------------------------------------ #
# Takes a single integer argument and converts it to a string of the form; "HH.MM.SS"  #
# Arguments:                                                                           #
#     1) integer seconds.                                                              #
# Returns:                                                                             #
#     String as described above.                                                       #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -ir SECONDS=${1}
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i  Remainder
    declare -i  Seconds
    declare -i  Minutes
    declare -i  Hours
    declare     Runtime=""
    # ---------------------------------- PROCEDURE ----------------------------------- #
    Hours=$(( SECONDS / 3600 ))
    Remainder=$(( SECONDS % 3600 ))
    Minutes=$(( Remainder / 60 ))
    Seconds=$(( Remainder % 60 ))
    Runtime="$(printf "%02d.%02d.%02d" "${Hours}" "${Minutes}" "${Seconds}")"
    printf "%s" "${Runtime}"
}
# ------------------------------------------------------------------------------------ #
function fnEcho () {
# ------------------------------------------------------------------------------------ #
# output the arguments using the printf builtin.                                       #
# Arguments:                                                                           #
#   1) data to be displayed                                                            #
# Returns:                                                                             #
#   none                                                                               #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    printf "%b" "${@}"
}
# ------------------------------------------------------------------------------------ #
function fnCopies () {
# ------------------------------------------------------------------------------------ #
# Repeats a string value a specified number of times.                                  #
# Arguments:                                                                           #
#     1) string_expression - a string of one or more characters.                       #
#     2) integer - number of time string is to be repeated.                            #
# Returns:                                                                             #
#     a string.                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    C="${1}"
    declare -i N=${2}
    declare    S="$(printf "%*s" ${N} "")"
    declare    T="${S// /${C}}"
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnEcho "${T}"
    return
}
# ------------------------------------------------------------------------------------ #
# ------------------------------------------------------------------------------------ #
function fnHR () {
# ------------------------------------------------------------------------------------ #
# Applies numfmt to the supplied integer numeric argument to make it human readable.   #
# Arguments: integer: number to be formatted.                                          #
# Returns: string: formatted number.                                                   #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r UNIT="iec"
    declare -r FORMAT="%8.3f"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=0
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnEcho "$(numfmt --to=${UNIT} --format=${FORMAT} ${1})"
    # SC2086: Double quote to prevent globbing and word splitting.
    # shellcheck disable=SC2086
    return ${Result}
}

function fnPC () {
# ------------------------------------------------------------------------------------ #
# Takes two integer arguments and calculates the percentage of the second indicated by #
# the first. If either argument is zero returns zero.                                  #
# Arguments:                                                                           #
#  1) integer: value that represents the fractional percentage amount                  #
#  2) integer: value that represents the 100 percent amount                            #
# Returns: integer: percentage that first argument is of the second                    #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    if [[ ($1 -eq 0) || ($2 -eq 0) ]]; then
        fnEcho "0"
    else
        fnEcho $(( ${1} * 100 / ${2} ))
    fi
    # SC2086: Double quote to prevent globbing and word splitting.
    # shellcheck disable=SC2086
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnGetProfileSourcePath () {
# ------------------------------------------------------------------------------------ #
# Extract the source path of the profile from the conf file and return it as a string. #
# Arguments: 1) string: name of profile.                                               #
# Returns: string: path to backup source directory                                     #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r CONFFILE="${HOME}/.duply/${1}/conf"
    declare -r SOURCE="SOURCE="
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    Source="$(grep "${SOURCE}" <"${CONFFILE}")"
    # ---------------------------------- PROCEDURE ----------------------------------- #
    Source="${Source/$SOURCE/}"
    fnEcho "${Source//\'/}"
    return
}
# ------------------------------------------------------------------------------------ #
function fnGetProfileTargetPath () {
# ------------------------------------------------------------------------------------ #
# Extract the target path of the profile from the conf file and return it as a string. #
# Arguments: 1) string: name of profile.                                               #
# Returns: string: path to backup target directory                                     #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r CONFFILE="${HOME}/.duply/${1}/conf"
    declare -r TARGET="TARGET='file://"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare    Target="$(grep "${TARGET}" <"${CONFFILE}")"
    # ---------------------------------- PROCEDURE ----------------------------------- #
    Target="${Target/${TARGET}/}"
    fnEcho "${Target//\'/}"
    return
}
# ------------------------------------------------------------------------------------ #
function fnCheckMounted () {
# ------------------------------------------------------------------------------------ #
# Check if a filesystem is mounted with the specified partition label, if not display  #
# a prompt and keep checking until it is. Times out and returns false after a number   #
# of tries.                                                                            #
# Arguments: string: partition label                                                   #
# Return: True if filesystem mounted and false if not.                                 #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r  PARTLABEL="${1}"
    declare -r  PROCMOUNTS="/proc/mounts"
    declare -ir MAXTRIES=10
    declare -ir WAITSECS=15
    declare -r  MSG1="Please attach device containing partition label %s\\\n"
    declare -r  MSG2="Waiting: %d attempts with %d seconds between each"
    declare -r  END1="\nTimed out waiting for filesystem with partition label %s\n"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result                       # used to return result of function
    declare -i Tries=0
    # ---------------------------------- PROCEDURE ----------------------------------- #
    Result=${TRUE}
    # is label in mounted filesystem list
    until grep "${PARTLABEL}" <"${PROCMOUNTS}" >/dev/null 2>&1; do
        if (( Tries == 0 )); then
            fnEcho "$(printf "${MSG1}" "${PARTLABEL}")"
            fnEcho "$(printf "${MSG2}" ${MAXTRIES} ${WAITSECS})"
        elif (( Tries >= MAXTRIES )); then
            fnEcho "$(printf "${END1}" "${PARTLABEL}")"
            Result=${FALSE}
            break
        fi
        (( Tries++ ))
        fnEcho "."
        sleep ${WAITSECS}
    done
    if (( Tries > 0 )); then
        fnEcho "\n"
    fi
    # SC2086: Double quote to prevent globbing and word splitting.
    # shellcheck disable=SC2086
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnFullBackup () {
# ------------------------------------------------------------------------------------ #
# Run duply in dry run mode and examine log to see if this will be a full backup.      #
# Arguments: string: name of profile                                                   #
# Returns: true if a full backup is to be run else false                               #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r FULLBKP="forcing full backup"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${FALSE}
    # ---------------------------------- PROCEDURE ----------------------------------- #
    duply ${1} bkp --verbosity=3 --dry-run | grep "${FULLBKP}" >/dev/null
    Result=$?
    # SC2086: Double quote to prevent globbing and word splitting.
    # shellcheck disable=SC2086
    return ${Result}
}
# ------------------------------------------------------------------------------------ #
function fnArchiveLogFiles () {
# ------------------------------------------------------------------------------------ #
# Adds any run logs for the current profile to a 7z archive file and, if successful,   #
# deletes the run log files. Intended to be invoked prior to profile backup.           #
# Arguments:                                                                           #
#    1) string: name of profile to be processed                                        #
#    2) string: path to where run logs are stored                                      #
# Returns: nothing                                                                     #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r PROFILE="${1}"
    declare -r LOGDIR="${2}"
    declare -r ACTIVELOG="${3}"
    declare -r ARCFILE="${LOGDIR}/${PROFILE}.${RUNLOG}s.7z"
    declare -r LOGPATTERN="${PROFILE}.*.${RUNLOG}"
    declare -r RUNLOGS="${LOGDIR}/${LOGPATTERN}"
    declare -r ARCHIVER="7z u -bso0 -bse1 -x!${ACTIVELOG}"
    declare -r MSG1="Updating archive file: %s\n"
    declare -r MSG2="Update failed\n"
    declare -r MSG3="Skip log file: %s\n"
    declare -r MSG4=" Add log file: %s\n"
    # ---------------------------------- VARIABLES ----------------------------------- #
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnEcho "\n"
    printf "${MSG1}" "${ARCFILE}"
    ${ARCHIVER} "${ARCFILE}" "${RUNLOGS}"   # update archive with new run logs
    if [[ $? -lt 1 ]]; then                 # if update ok then delete run logs
        for LogFile in $(find ${LOGDIR} -maxdepth 1 -name "${LOGPATTERN}" -print); do
            if [[ "${ACTIVELOG}" == "${LogFile}" ]]; then
                printf "${MSG3}" "$(basename ${LogFile})"
            else
                rm -f "${LogFile}"
                printf "${MSG4}" "$(basename ${LogFile})"
            fi
        done
    else
        fnEcho "${MSG2}"
    fi
    fnEcho "\n"
    return
}
# ------------------------------------------------------------------------------------ #
function fnBackup () {
# ------------------------------------------------------------------------------------ #
# Run duply to perform a backup for the specified profile. If a full backup is invoked #
# by duply then a progress bar is displayed.                                           #
# Arguments:                                                                           #
#    1) string: name of profile to be processed                                        #
# Returns: none                                                                        #
# ------------------------------------------------------------------------------------ #
    # ---------------------------------- CONSTANTS ----------------------------------- #
    declare -r PROFILE="${1}"
    declare -r SEPARATOR="\n$(fnCopies "~" 80)\n"
    declare -r PROGRESS="$(fnCopies "#" 50)"
    declare -r BAR="$(fnCopies "-" 50)"
    declare -r LOGFILE="${PROFILE}.$(fnTS).run.log"
    declare -r FULL="Full backup invoked.\n"
    declare -r INCREMENTAL="Incremental backup invoked.\n"
    declare -r RUNLOGS="RunLogs"
    # ---------------------------------- VARIABLES ----------------------------------- #
    declare -i Result=${TRUE}
    declare -i Full=${TRUE}
    declare -i DupStart
    declare -i DupEnd
    declare -i DupTime
    declare -i Done=0
    declare -i Progress=0
    declare -i Elapsed=0
    declare -i Runtime=0
    declare -i Timeleft=0
    declare -i TotalFiles=0
    declare -i TotalSize=0
    declare -i FileCount=0
    declare -i ByteCount=0
    declare -i ByteSize=0
    declare    Filename=""
    declare    DupLine=""
    declare    ProgressBar=""
    declare    Source=""
    declare    Target=""
    declare    Header=""
    declare    LogDir=""
    declare    LogFile=""
    # ---------------------------------- PROCEDURE ----------------------------------- #
    fnEcho "${SEPARATOR}"
    if fnCheckMounted "${PROFILE}"; then
        Source="$(fnGetProfileSourcePath "${PROFILE}")"
        Target="$(fnGetProfileTargetPath "${PROFILE}")"
        LogDir="$(dirname ${Target})/${RUNLOGS}"
        LogFile="${LogDir}/${LOGFILE}"
        fnArchiveLogFiles "${PROFILE}" "${LogDir}" "${LOGFILE}" >> "${LogFile}"
        Header="${PROFILE}:Backup of '${Source}' commencing..."
        fnEcho "${Header}\n"
        DupStart=$(date +"%s")
        fnEcho "Run log is ${LogFile}\n"
        duply ${PROFILE} cleanup >${LogFile} 2>&1
        fnFullBackup "${PROFILE}"
        Full=${?}
        if (( Full == TRUE )); then
            TotalFiles=$(find "${Source}" -type f -or -type d -or -type l | wc -l)
            TotalSize=$(du -bs "${Source}" | cut -f1)
            fnEcho "${FULL}"
        else
            fnEcho "${INCREMENTAL}"
        fi
        while IFS= read -r DupLine; do
            if (( Full == TRUE )); then
                if [[ "${DupLine:0:2}" == "A " ]]; then
                    Filename="${Source}/${DupLine:2}"
                    if [[ -e "${Filename}" ]]; then
                        DupTime=$(date +"%s")
                        Elapsed=$(( DupTime - DupStart ))
                        (( FileCount++ ))
                        ByteSize=$(stat -c%s "${Filename}")
                        (( ByteCount += ByteSize ))
                        Done=$(fnPC ByteCount TotalSize)
                        Progress=$(( Done / 2 ))
                        if (( Done == 0 ));then
                            Runtime=0
                            Timeleft=0
                        else
                            Runtime=$(( Elapsed * 100 / Done ))
                            Timeleft=$(( Runtime - Elapsed ))
                        fi
                        ProgressBar="Run:$(fnHMS ${Elapsed}) "
                        ProgressBar+="Files:$(fnHR ${FileCount})/$(fnHR ${TotalFiles}) "
                        ProgressBar+="${Done}% "
                        ProgressBar+="[${PROGRESS:0:${Progress}}${BAR:${Progress}}] "
                        ProgressBar+="$(fnHR ${ByteCount})/$(fnHR ${TotalSize}) Bytes "
                        ProgressBar+="ETA:$(fnHMS ${Timeleft}) "
                        ProgressBar+="Est.:$(fnHMS ${Runtime})"
                        # print composite progress bar, carriage return but no line feed
                        fnEcho "\r${ProgressBar}"
                        continue
                    fi
                fi
            fi
            fnEcho "${DupLine}\n" >> ${LogFile}
        done < <(duply ${PROFILE} bkp 2>&1)
    else
        Result=${FALSE}
    fi
    # SC2086: Double quote to prevent globbing and word splitting.
    # shellcheck disable=SC2086
    return ${Result}
}
# ==================================================================================== #
#                                       M A I N                                        #
# ==================================================================================== #
#
# -------------------------------- GLOBAL CONSTANTS ---------------------------------- #
declare -ir TRUE=0
declare -ir FALSE=1
declare -r  NL=$'\n'
declare -r  SCRIPTNAME="${0##*/}"          # base name of script
declare -r  RUNLOG="run.log"
declare -r  RUNDATE="$(date +%Y%m%d)"
declare -r  RUNDATIME="${RUNDATE}$(date +%H%M)"
declare -r  BACKUPS="Backups"
declare -r  ERRMSG="ERROR: Could not find backup filesystem '${BACKUPS}'"
declare -r  DEFAULT="Routine Backup Run"   # default value for backup description
declare -r  COMMENT="${@:-${DEFAULT}}"     # if present use command line arguments as
                                           # a backup comment
declare -r  DESCHDR="# Description: "      # header string prefix used for descriptions
# -------------------------------- GLOBAL VARIABLES ---------------------------------- #
declare -a  Profiles
declare     Manifest                       # holds file specification for manifest files
declare     WorkPath                       # used to construct a filespec when finding
                                           # the latest manifest files
declare -i  Result=${TRUE}
# --------------------------------- MAIN PROCEDURE ----------------------------------- #
if fnCheckMounted "${BACKUPS}"; then
    pushd ${HOME} >/dev/null 2>&1
    # extract list of profile directories from the duply config directory
    Profiles=($(find "$HOME/.duply/" -type d -exec echo "{}" \; | \
                grep -v "/$" | grep -v example | sed "s#${HOME}/.duply/##" | sort))
    for Profile in "${Profiles[@]}"; do
        # run duply backup for each profile
        if fnBackup "${Profile}"; then
            # dump ok so append a comment line to the manifest file
            # get the target path for this profile
            WorkPath=$(fnGetProfileTargetPath "${Profile}")
            # get the file specification for the manifest created this run
            Manifest=$(ls -t "${WorkPath}" | grep "manifest" | head -n1)
            # append the run comments
            printf "%s\n" "${DESCHDR}${COMMENT}" >> "${WorkPath}/${Manifest}"
        else
            Result=${FALSE}                 # flag and exit on any error
            break
        fi
    done
    fnEcho ${NL}${NL}
    popd >/dev/null 2>&1
else
    Result=${FALSE}
fi
exit ${Result}
